package com.secure.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.secure.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findByUsername(String name);

}
